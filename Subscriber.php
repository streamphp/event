<?php

/**
 * This File is part of the Stream\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class: Subscriber
 *
 * @see EventSubscriberInterface
 */
abstract class Subscriber implements EventSubscriberInterface
{

    /**
     * getSubscriptions
     *
     * @static
     * @return array
     */
    protected static function getSubscriptions()
    {
        return [];
    }

    /**
     * getSubscribedEvents
     *
     * @static
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return static::getSubscriptions();
    }
}
