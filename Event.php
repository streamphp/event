<?php

/**
 * This File is part of the Stream\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Event;

use Symfony\Component\EventDispatcher\Event as SymEvent;

/**
 * Class: Event
 *
 * @uses SymEvent
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Event extends SymEvent
{

    /**
     * data
     * @var array
     */
    protected $data;

    /**
     * __construct
     *
     * @param array $data
     */
    public function __construct($name, array $data = [])
    {
        $this->setName($name);
        $this->data = $data;
    }

    /**
     * Getter for event data
     *
     * @param mixed $prop
     * @return mixed
     */
    public function __get($prop)
    {
        if (isset($this->data[$prop])) {
            return $this->data[$prop];
        }
    }

    /**
     * Setter for event data
     *
     * Readonly event in this case
     *
     * @param mixed $prop
     * @param mixed $val
     */
    public function __set($prop, $val)
    {
    }
}
