<?php

/**
 * This File is part of package name
 *
 * (c) author <email>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Stream\Event\Dispatcher;
//use Stream\Event\Event;
//use Stream\Event\Subscriber;
use \Mockery as m;

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ClassName
     */
    protected $dispatcher;

    protected function tearDown()
    {
        m::close();
    }

    /**
     * @test
     * @covers Dispatcher#on()
     * @covers Dispatcher#trigger()
     */
    public function testDispatcherAddListener()
    {
        $test = 0;
        $this->makeDispatcher();

        $this->dispatcher->on('event', function ($context) use (&$test) {
            $test++;
            return 'bar';
        });
        $this->dispatcher->trigger('event');
        $this->assertEquals(1, $test);
    }


    /**
     * @test
     * @covers Dispatcher#on()
     * @covers Dispatcher#trigger()
     */
    public function testAddListenerUseNamespaces()
    {
        $test = 0;
        $this->makeDispatcher();

        $this->dispatcher->on('event', function ($context) use (&$test) { $test++; });
        $this->dispatcher->on('event.foo', function ($context) use (&$test) { $test++; });
        $this->dispatcher->on('foobar', function ($context) use (&$test) { return 'bar'; });

        $this->dispatcher->trigger('event.baz');
        $this->dispatcher->trigger('event.boo');
        $this->dispatcher->trigger('event.gom');

        $this->assertEquals(0, $test);
        $this->dispatcher->trigger('event');
        $this->assertEquals(2, $test);
        $this->dispatcher->trigger('event.bar');
        $this->assertEquals(2, $test);
        $this->dispatcher->trigger('event.foo');
        $this->assertEquals(3, $test);
    }

    /**
     * @test
     * @covers Dispatcher#on()
     * @covers Dispatcher#off()
     */
    public function testRemoveListener()
    {
        $test = 0;
        $this->makeDispatcher();

        $this->dispatcher->on('event', function ($context) use (&$test) {
            $test++;
        });
        $l = function ($context) use (&$test) {
            $test++;
        };

        $this->dispatcher->on('event', $l);
        $this->dispatcher->trigger('event');
        $this->assertEquals(2, $test);
        $this->dispatcher->off('event', $l);
        $this->dispatcher->trigger('event');
        $this->assertEquals(3, $test);
    }

    /**
     * @test
     * @covers Dispatcher#trigger()
     */
    public function testFireProirotyOrder()
    {
        $this->makeDispatcher();

        $this->dispatcher->on('event', function () {
            return 'c';
        }, 11);

        $this->dispatcher->on('event', function () {
            return 'a';
        }, 10);

        $this->dispatcher->on('event', function () {
            return 'b';
        }, 0);

        $results = $this->dispatcher->trigger('event');

        $this->assertEquals('b', $results[0]);
        $this->assertEquals('a', $results[1]);
        $this->assertEquals('c', $results[2]);
    }

    /**
     * @test
     * @covers Dispatcher#hasListeners()
     */
    public function testHasListener()
    {
        $this->makeDispatcher();

        $this->dispatcher->on('event', function () { return false; });
        $this->dispatcher->on('event.test', function () { return false; });

        $this->assertTrue($this->dispatcher->hasListeners());
        $this->dispatcher->off('event');
        $this->assertFalse($this->dispatcher->hasListeners());
        $this->assertFalse($this->dispatcher->hasListeners('event'));

        $listener = function () { return 'foo'; };
        $this->dispatcher->on('foo', $listener);
        $this->assertTrue($this->dispatcher->hasListeners('foo'));
        $this->dispatcher->off('foo', $listener);
        $this->assertFalse($this->dispatcher->hasListeners('foo'));
    }

    /**
     * @test
     * @covers Dispatcher#once()
     */
    public function testBindOnce()
    {
        $this->makeDispatcher();
        $this->dispatcher->once('event', function ($evt) { return 'a'; });

        $results = $this->dispatcher->trigger('event');
        $this->assertEquals(1, count($results));

        $results = $this->dispatcher->trigger('event');
        $this->asserttrue(is_null($results));
        $results = $this->dispatcher->trigger('event');
    }
    /**
     * @test
     * @covers Dispatcher#trigger()
     */
    public function testEventFireStopPropageation()
    {
        $this->makeDispatcher();
        $event = m::mock('alias:Stream\Event\Event');
        $event->shouldReceive('isPropagationStopped')->times(2)->andReturn(false, true);

        $this->dispatcher->on('event', function ($evt) { return 'a'; });
        $this->dispatcher->on('event', function ($evt) { return 'b'; });
        $this->dispatcher->on('event', function ($evt) { return 'c'; });

        $results = $this->dispatcher->trigger('event', $event);
        $this->assertEquals(2, count($results));
    }

    /**
     * @test
     * @covers Dispatcher#bindClassCallback()
     */
    public function testBindClassCallback()
    {
        $tc = m::mock('TestClass');
        $tc->shouldReceive('eventCallback')->times(1)->andReturn('foo');
        $container = m::mock('\Stream\IoC\Container');
        $container->shouldReceive('resolve')->times(1)->andReturn($tc);

        $this->makeDispatcher($container);
        $this->dispatcher->on('event', 'TestClass@eventCallback');

        $results = $this->dispatcher->trigger('event');
        $this->assertEquals('foo', $results[0]);

    }

    /**
     * @test
     * @covers Dispatcher#getListeners()
     */
    public function testGetListener()
    {
        $this->makeDispatcher();
        $listener = function () { return 'foo'; };
        $this->dispatcher->on('event', $listener);
        $listeners = $this->dispatcher->getListeners('event');
        $this->assertEquals($listener, $listeners[0]['callback']);

        $listeners = $this->dispatcher->getListeners();
        $this->assertEquals($listener, $listeners[0]['callback']);
    }


    protected function makeDispatcher($container = null)
    {
        if (is_null($container)) {
            $container = m::mock('Stream\IoC\Container');
        }
        $this->dispatcher = new Dispatcher($container);
    }

    /**
     * @test
     */
    public function testSubscriberOn()
    {
        $events = array(
            'event.response' => array(
                array('onEventResponsePre', 10),
                array('onEventResponsePost', 0),
            ),
            'event.order'     => array('onStoreOrder', 0),
        );
        $subscriber = $this->getMock('Stream\Event\Subscriber', array('getSubscribedEvents', 'onEventResponsePre', 'onEventResponsePost', 'onStoreOrder'));
        $subscriber::staticExpects($this->any())->method('getSubscribedEvents')->will($this->returnValue($events));

        $subscriber->expects($this->any())->method('onEventResponsePre')->will($this->returnValue('foo'));
        $subscriber->expects($this->any())->method('onEventResponsePost')->will($this->returnValue('bar'));
        $subscriber->expects($this->any())->method('onStoreOrder')->will($this->returnValue('baz'));


        $this->makeDispatcher();
        $this->dispatcher->addSubscriber($subscriber);
        $results = $this->dispatcher->trigger('event.response');

        $this->assertEquals('bar', $results[0]);
        $this->assertEquals('foo', $results[1]);

        $results = $this->dispatcher->trigger('event.order');
        $this->assertEquals('baz', $results[0]);

        $results = $this->dispatcher->trigger('event.response');
        $this->assertFalse(empty($results));

        $this->dispatcher->removeSubscriber($subscriber);
        $results = $this->dispatcher->trigger('event.response');

        $this->assertTrue(empty($results));

        $results = $this->dispatcher->trigger('event.order');
        $this->assertTrue(empty($results));

    }
}
